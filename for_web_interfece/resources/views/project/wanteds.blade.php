@extends('layouts.default')

@section('title', 'Wanted Employees')

@push('css')
<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="/assets/plugins/datatables.net-fixedcolumns-bs4/css/fixedcolumns.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')
<!-- begin breadcrumb -->
<ol class="breadcrumb float-xl-right">
  <li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
  <li class="breadcrumb-item active">Wanted Employees</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Wanted Employees</h1>
<!-- end page-header -->
<!-- begin row -->
<div class="row">
  <!-- begin col-10 -->
  <div class="col-xl-12">
    <!-- begin panel -->
    <div class="panel panel-inverse">
      <!-- begin panel-heading -->
      <div class="panel-heading">
        <h4 class="panel-title">Wanted Employees</h4>
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
              class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
              class="fa fa-redo"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
              class="fa fa-minus"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i
              class="fa fa-times"></i></a>
        </div>
      </div>
      <!-- end panel-heading -->
      <!-- begin panel-body -->
      <div class="panel-body">
        <a href="#modal-without-animation" data-toggle="modal" class="btn btn-primary">Search an Employee</a>
        <table id="data-table-fixed-columns" class="table table-striped table-bordered table-td-valign-middle">
          <thead>
            <tr>
              <th width="1%"></th>
              <th width="1%" data-orderable="false"></th>
              <th class="text-nowrap">Employee Name</th>
              <th class="text-nowrap">Date</th>
              <th class="text-nowrap">Status</th>
              <th class="text-nowrap"></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($list as $index=>$item)

            <tr class="odd gradeX">
              <td width="1%" class="f-s-600 text-inverse">{{ $index+1 }}</td>
              <td width="1%" class="with-img"><img src="{{ $item->employee->image }}" class="img-rounded height-30" />
              </td width="3%">
              <td>{{ $item->employee->name }}</td>
              <td>{{ $item->date }}</td>
              <td>
                <center>
                  @if (!$item->status)
                  <a href="javascript:;" class="btn btn-success items-center">Completed</a>
                  @else
                  <a href="javascript:;" class="btn btn-warning items-center">Searching Continue</a>
                  @endif
                </center>
              </td>
              <td>
                <center>
                  <a href="{{ route('admin.employee-history', $item->id) }}" class="btn btn-info"><i
                      class="fa fa-eye"></i></a>
                  @if ($item->status)
                  <a href="{{ route('admin.stop-searching', $item->id) }}" class="btn btn-danger"><i
                      class="fa fa-square"></i></a>
                  @endif
                </center>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- end panel-body -->
    </div>
    <!-- end panel -->
  </div>
  <!-- end col-10 -->
</div>
<!-- end row -->

<div class="modal fade" id="modal-without-animation">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Select an Employee for searching</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form action="{{ route('admin.search') }}" method="POST" class="d-flex">
          @csrf
          <select name="id" class="form-control mr-2">
            @foreach ($employees as $item)
            <option value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
          </select>

          <button class="btn btn-primary">Search</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables.net-fixedcolumns/js/dataTables.fixedcolumns.min.js"></script>
<script src="/assets/plugins/datatables.net-fixedcolumns-bs4/js/fixedcolumns.bootstrap4.min.js"></script>
<script src="/assets/js/demo/table-manage-fixed-columns.demo.js"></script>
@endpush