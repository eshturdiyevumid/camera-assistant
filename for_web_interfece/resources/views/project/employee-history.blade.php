@extends('layouts.default')

@section('title', 'Employee History')

@push('css')
<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="/assets/plugins/datatables.net-fixedcolumns-bs4/css/fixedcolumns.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')
<!-- begin breadcrumb -->
<ol class="breadcrumb float-xl-right">
  <li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
  <li class="breadcrumb-item active">History of {{ $wanted->employee->name }}</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Wanted Employees' History</h1>
<!-- end page-header -->
<!-- begin row -->
<div class="row">
  <!-- begin col-10 -->
  <div class="col-xl-12">
    <!-- begin panel -->
    <div class="panel panel-inverse">
      <!-- begin panel-heading -->
      <div class="panel-heading">
        <h4 class="panel-title">Employee History</h4>
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
              class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
              class="fa fa-redo"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
              class="fa fa-minus"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i
              class="fa fa-times"></i></a>
        </div>
      </div>
      <!-- end panel-heading -->
      <!-- begin panel-body -->
      <div class="panel-body">
        <table id="data-table-fixed-columns" class="table table-striped table-bordered table-td-valign-middle">
          <thead>
            <tr>
              <th width="1%"></th>
              <th width="1%" data-orderable="false"></th>
              <th class="text-nowrap">Employee Name</th>
              <th class="text-nowrap">Territory</th>
              <th class="text-nowrap">Action Type</th>
              <th class="text-nowrap">Action Time</th>
              <th class="text-nowrap">Device</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($wanted->history as $index=>$item)
            <tr class="odd gradeX">
              <td width="1%" class="f-s-600 text-inverse">{{ $index+1 }}</td>
              <td width="1%" class="with-img"><img src="{{ $wanted->employee->image }}" class="img-rounded height-30" />
              </td>
              <td>{{ $wanted->employee->name }}</td>
              <td>{{ $item->device->territory->name }}</td>
              <td>{{ $item->device->action_type }}</td>
              <td>{{ $item->action_date }}</td>
              <td>{{ $item->device->name }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- end panel-body -->
    </div>
    <!-- end panel -->
  </div>
  <!-- end col-10 -->
</div>
<!-- end row -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables.net-fixedcolumns/js/dataTables.fixedcolumns.min.js"></script>
<script src="/assets/plugins/datatables.net-fixedcolumns-bs4/js/fixedcolumns.bootstrap4.min.js"></script>
<script src="/assets/js/demo/table-manage-fixed-columns.demo.js"></script>
@endpush