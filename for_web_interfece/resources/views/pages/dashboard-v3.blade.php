@extends('layouts.default')

@section('title', 'Dashboard')

@push('css')
<link href="/assets/plugins/jvectormap-next/jquery-jvectormap.css" rel="stylesheet" />
<link href="/assets/plugins/nvd3/build/nv.d3.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@endpush

@push('css')
<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="/assets/plugins/datatables.net-fixedcolumns-bs4/css/fixedcolumns.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')
<!-- begin breadcrumb -->
<ol class="breadcrumb float-xl-right">
	<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
	<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
	<li class="breadcrumb-item active">Dashboard</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header mb-3">Dashboard</h1>
<!-- end page-header -->
<!-- begin daterange-filter -->
<div class="d-sm-flex align-items-center mb-3">
	<a href="#" class="btn btn-inverse mr-2 text-truncate" id="daterange-filter">
		<i class="fa fa-calendar fa-fw text-white-transparent-5 ml-n1"></i>
		<span>1 Jun 2020 - 7 Jun 2020</span>
		<b class="caret"></b>
	</a>
	<div class="text-muted f-w-600 mt-2 mt-sm-0">compared to <span id="daterange-prev-date">24 Mar-30 Apr 2020</span>
	</div>
</div>
<!-- end daterange-filter -->
<!-- begin row -->
<div class="row">
	<!-- begin col-6 -->
	<div class="col-xl-6">
		<!-- begin card -->
		<div class="card border-0 bg-dark text-white mb-3 overflow-hidden">
			<!-- begin card-body -->
			<div class="card-body">
				<!-- begin row -->
				<div class="row">
					<!-- begin col-7 -->
					<div class="col-xl-7 col-lg-8">
						<!-- begin title -->
						<div class="mb-3 text-grey">
							<b>TOTAL SALES</b>
							<span class="ml-2">
								<i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover" data-title="Total sales"
									data-placement="top"
									data-content="Net sales (gross sales minus discounts and returns) plus taxes and shipping. Includes orders from all sales channels."></i>
							</span>
						</div>
						<!-- end title -->
						<!-- begin total-sales -->
						<div class="d-flex mb-1">
							<h2 class="mb-0">$<span data-animation="number" data-value="64559.25">0.00</span></h2>
							<div class="ml-auto mt-n1 mb-n1">
								<div id="total-sales-sparkline"></div>
							</div>
						</div>
						<!-- end total-sales -->
						<!-- begin percentage -->
						<div class="mb-3 text-grey">
							<i class="fa fa-caret-up"></i> <span data-animation="number" data-value="33.21">0.00</span>% compare to
							last week
						</div>
						<!-- end percentage -->
						<hr class="bg-white-transparent-2" />
						<!-- begin row -->
						<div class="row text-truncate">
							<!-- begin col-6 -->
							<div class="col-6">
								<div class="f-s-12 text-grey">Total sales order</div>
								<div class="f-s-18 m-b-5 f-w-600 p-b-1" data-animation="number" data-value="1568">0</div>
								<div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
									<div class="progress-bar progress-bar-striped rounded-right bg-teal" data-animation="width"
										data-value="55%" style="width: 0%"></div>
								</div>
							</div>
							<!-- end col-6 -->
							<!-- begin col-6 -->
							<div class="col-6">
								<div class="f-s-12 text-grey">Avg. sales per order</div>
								<div class="f-s-18 m-b-5 f-w-600 p-b-1">$<span data-animation="number" data-value="41.20">0.00</span>
								</div>
								<div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
									<div class="progress-bar progress-bar-striped rounded-right" data-animation="width" data-value="55%"
										style="width: 0%"></div>
								</div>
							</div>
							<!-- end col-6 -->
						</div>
						<!-- end row -->
					</div>
					<!-- end col-7 -->
					<!-- begin col-5 -->
					<div class="col-xl-5 col-lg-4 align-items-center d-flex justify-content-center">
						<img src="/assets/img/svg/img-1.svg" height="150px" class="d-none d-lg-block" />
					</div>
					<!-- end col-5 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end card-body -->
		</div>
		<!-- end card -->
	</div>
	<!-- end col-6 -->
	<!-- begin col-6 -->
	<div class="col-xl-6">
		<!-- begin row -->
		<div class="row">
			<!-- begin col-6 -->
			<div class="col-sm-6">
				<!-- begin card -->
				<div class="card border-0 bg-dark text-white text-truncate mb-3">
					<!-- begin card-body -->
					<div class="card-body">
						<!-- begin title -->
						<div class="mb-3 text-grey">
							<b class="mb-3">CONVERSION RATE</b>
							<span class="ml-2"><i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover"
									data-title="Conversion Rate" data-placement="top"
									data-content="Percentage of sessions that resulted in orders from total number of sessions."
									data-original-title="" title=""></i></span>
						</div>
						<!-- end title -->
						<!-- begin conversion-rate -->
						<div class="d-flex align-items-center mb-1">
							<h2 class="text-white mb-0"><span data-animation="number" data-value="2.19">0.00</span>%</h2>
							<div class="ml-auto">
								<div id="conversion-rate-sparkline"></div>
							</div>
						</div>
						<!-- end conversion-rate -->
						<!-- begin percentage -->
						<div class="mb-4 text-grey">
							<i class="fa fa-caret-down"></i> <span data-animation="number" data-value="0.50">0.00</span>% compare to
							last week
						</div>
						<!-- end percentage -->
						<!-- begin info-row -->
						<div class="d-flex mb-2">
							<div class="d-flex align-items-center">
								<i class="fa fa-circle text-red f-s-8 mr-2"></i>
								Added to cart
							</div>
							<div class="d-flex align-items-center ml-auto">
								<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> <span data-animation="number"
										data-value="262">0</span>%</div>
								<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number"
										data-value="3.79">0.00</span>%</div>
							</div>
						</div>
						<!-- end info-row -->
						<!-- begin info-row -->
						<div class="d-flex mb-2">
							<div class="d-flex align-items-center">
								<i class="fa fa-circle text-warning f-s-8 mr-2"></i>
								Reached checkout
							</div>
							<div class="d-flex align-items-center ml-auto">
								<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> <span data-animation="number"
										data-value="11">0</span>%</div>
								<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number"
										data-value="3.85">0.00</span>%</div>
							</div>
						</div>
						<!-- end info-row -->
						<!-- begin info-row -->
						<div class="d-flex">
							<div class="d-flex align-items-center">
								<i class="fa fa-circle text-lime f-s-8 mr-2"></i>
								Sessions converted
							</div>
							<div class="d-flex align-items-center ml-auto">
								<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> <span data-animation="number"
										data-value="57">0</span>%</div>
								<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number"
										data-value="2.19">0.00</span>%</div>
							</div>
						</div>
						<!-- end info-row -->
					</div>
					<!-- end card-body -->
				</div>
				<!-- end card -->
			</div>
			<!-- end col-6 -->
			<!-- begin col-6 -->
			<div class="col-sm-6">
				<!-- begin card -->
				<div class="card border-0 bg-dark text-white text-truncate mb-3">
					<!-- begin card-body -->
					<div class="card-body">
						<!-- begin title -->
						<div class="mb-3 text-grey">
							<b class="mb-3">STORE SESSIONS</b>
							<span class="ml-2"><i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover"
									data-title="Store Sessions" data-placement="top"
									data-content="Number of sessions on your online store. A session is a period of continuous activity from a visitor."
									data-original-title="" title=""></i></span>
						</div>
						<!-- end title -->
						<!-- begin store-session -->
						<div class="d-flex align-items-center mb-1">
							<h2 class="text-white mb-0"><span data-animation="number" data-value="70719">0</span></h2>
							<div class="ml-auto">
								<div id="store-session-sparkline"></div>
							</div>
						</div>
						<!-- end store-session -->
						<!-- begin percentage -->
						<div class="mb-4 text-grey">
							<i class="fa fa-caret-up"></i> <span data-animation="number" data-value="9.5">0.00</span>% compare to last
							week
						</div>
						<!-- end percentage -->
						<!-- begin info-row -->
						<div class="d-flex mb-2">
							<div class="d-flex align-items-center">
								<i class="fa fa-circle text-teal f-s-8 mr-2"></i>
								Mobile
							</div>
							<div class="d-flex align-items-center ml-auto">
								<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> <span data-animation="number"
										data-value="25.7">0.00</span>%</div>
								<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="53210">0</span>
								</div>
							</div>
						</div>
						<!-- end info-row -->
						<!-- begin info-row -->
						<div class="d-flex mb-2">
							<div class="d-flex align-items-center">
								<i class="fa fa-circle text-blue f-s-8 mr-2"></i>
								Desktop
							</div>
							<div class="d-flex align-items-center ml-auto">
								<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> <span data-animation="number"
										data-value="16.0">0.00</span>%</div>
								<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="11959">0</span>
								</div>
							</div>
						</div>
						<!-- end info-row -->
						<!-- begin info-row -->
						<div class="d-flex">
							<div class="d-flex align-items-center">
								<i class="fa fa-circle text-aqua f-s-8 mr-2"></i>
								Tablet
							</div>
							<div class="d-flex align-items-center ml-auto">
								<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> <span data-animation="number"
										data-value="7.9">0.00</span>%</div>
								<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="5545">0</span>
								</div>
							</div>
						</div>
						<!-- end info-row -->
					</div>
					<!-- end card-body -->
				</div>
				<!-- end card -->
			</div>
			<!-- end col-6 -->
		</div>
		<!-- end row -->
	</div>
	<!-- end col-6 -->
</div>
<!-- end row -->
<!-- begin row -->
<div class="row">
	<!-- begin col-8 -->
	<div class="col-xl-8 col-lg-6">
		<!-- begin card -->
		<div class="card bg-dark border-0 text-white mb-3">
			<div class="card-body">
				<div class="mb-3 text-grey"><b>VISITORS ANALYTICS</b> <span class="ml-2"><i class="fa fa-info-circle"
							data-toggle="popover" data-trigger="hover" data-title="Top products with units sold" data-placement="top"
							data-content="Products with the most individual units sold. Includes orders from all sales channels."
							data-original-title="" title=""></i></span></div>
				<div class="row">
					<div class="col-xl-3 col-4">
						<h3 class="mb-1"><span data-animation="number" data-value="127.1">0</span>K</h3>
						<div>New Visitors</div>
						<div class="text-grey f-s-11 text-truncate"><i class="fa fa-caret-up"></i> <span data-animation="number"
								data-value="25.5">0.00</span>% from previous 7 days</div>
					</div>
					<div class="col-xl-3 col-4">
						<h3 class="mb-1"><span data-animation="number" data-value="179.9">0</span>K</h3>
						<div>Returning Visitors</div>
						<div class="text-grey f-s-11 text-truncate"><i class="fa fa-caret-up"></i> <span data-animation="number"
								data-value="5.33">0.00</span>% from previous 7 days</div>
					</div>
					<div class="col-xl-3 col-4">
						<h3 class="mb-1"><span data-animation="number" data-value="766.8">0</span>K</h3>
						<div>Total Page Views</div>
						<div class="text-grey f-s-11 text-truncate"><i class="fa fa-caret-up"></i> <span data-animation="number"
								data-value="0.323">0.00</span>% from previous 7 days</div>
					</div>
				</div>
			</div>
			<div class="card-body p-0">
				<div style="height: 269px">
					<div id="visitors-line-chart" class="widget-chart-full-width nvd3-inverse-mode" style="height: 254px"></div>
				</div>
			</div>
		</div>
		<!-- end card -->
	</div>
	<!-- end col-8 -->
	<!-- begin col-4 -->
	<div class="col-xl-4 col-lg-6">
		<!-- begin card -->
		<div class="card bg-dark border-0 text-white mb-3">
			<div class="card-body">
				<div class="mb-2 text-grey">
					<b>SESSION BY LOCATION</b>
					<span class="ml-2"><i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover"
							data-title="Total sales" data-placement="top"
							data-content="Net sales (gross sales minus discounts and returns) plus taxes and shipping. Includes orders from all sales channels."></i></span>
				</div>
				<div id="visitors-map" class="mb-2" style="height: 200px"></div>
				<div>
					<div class="d-flex align-items-center text-white mb-2">
						<div class="widget-img widget-img-xs rounded bg-inverse mr-2 width-40"
							style="background-image: url(/assets/img/flag/us.jpg)"></div>
						<div class="d-flex w-100">
							<div>United States</div>
							<div class="ml-auto text-grey"><span data-animation="number" data-value="39.85">0.00</span>%</div>
						</div>
					</div>
					<div class="d-flex align-items-center text-white mb-2">
						<div class="widget-img widget-img-xs rounded bg-inverse mr-2 width-40"
							style="background-image: url(/assets/img/flag/cn.jpg)"></div>
						<div class="d-flex w-100">
							<div>China</div>
							<div class="ml-auto text-grey"><span data-animation="number" data-value="14.23">0.00</span>%</div>
						</div>
					</div>
					<div class="d-flex align-items-center text-white mb-2">
						<div class="widget-img widget-img-xs rounded bg-inverse mr-2 width-40"
							style="background-image: url(/assets/img/flag/de.jpg)"></div>
						<div class="d-flex w-100">
							<div>Germany</div>
							<div class="ml-auto text-grey"><span data-animation="number" data-value="12.83">0.00</span>%</div>
						</div>
					</div>
					<div class="d-flex align-items-center text-white mb-2">
						<div class="widget-img widget-img-xs rounded bg-inverse mr-2 width-40"
							style="background-image: url(/assets/img/flag/fr.jpg)"></div>
						<div class="d-flex w-100">
							<div>France</div>
							<div class="ml-auto text-grey"><span data-animation="number" data-value="11.14">0.00</span>%</div>
						</div>
					</div>
					<div class="d-flex align-items-center text-white mb-0">
						<div class="widget-img widget-img-xs rounded bg-inverse mr-2 width-40"
							style="background-image: url(/assets/img/flag/jp.jpg)"></div>
						<div class="d-flex w-100">
							<div>Japan</div>
							<div class="ml-auto text-grey"><span data-animation="number" data-value="10.75">0.00</span>%</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end card -->
	</div>
	<!-- end col-4 -->
</div>
<div class="panel panel-inverse">
	<!-- begin panel-heading -->
	<div class="panel-heading">
		<h4 class="panel-title">Action History</h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
					class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
					class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
					class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i
					class="fa fa-times"></i></a>
		</div>
	</div>
	<!-- end panel-heading -->
	<!-- begin panel-body -->
	<div class="panel-body">
		<table id="data-table-fixed-columns" class="table table-striped table-bordered table-td-valign-middle">
			<thead>
				<tr>
					<th width="1%"></th>
					<th width="1%" data-orderable="false"></th>
					<th class="text-nowrap">Employee Name</th>
					<th class="text-nowrap">Territory</th>
					<th class="text-nowrap">Action Type</th>
					<th class="text-nowrap">Action Time</th>
					<th class="text-nowrap">Device</th>
				</tr>
			</thead>
			<tbody>
				<tr class="odd gradeX">
					<td width="1%" class="f-s-600 text-inverse">1</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>
					<td>Trident</td>
					<td>Internet Explorer 4.0</td>
					<td>Win 95+</td>
					<td>4</td>
					<td>X</td>
				</tr>
				<tr class="even gradeC">
					<td width="1%" class="f-s-600 text-inverse">2</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-2.jpg" class="img-rounded height-30" /></td>
					<td>Trident</td>
					<td>Internet Explorer 5.0</td>
					<td>Win 95+</td>
					<td>5</td>
					<td>C</td>
				</tr>
				<tr class="odd gradeA">
					<td width="1%" class="f-s-600 text-inverse">3</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-3.jpg" class="img-rounded height-30" /></td>
					<td>Trident</td>
					<td>Internet Explorer 5.5</td>
					<td>Win 95+</td>
					<td>5.5</td>
					<td>A</td>
				</tr>
				<tr class="even gradeA">
					<td width="1%" class="f-s-600 text-inverse">4</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-4.jpg" class="img-rounded height-30" /></td>
					<td>Trident</td>
					<td>Internet Explorer 6</td>
					<td>Win 98+</td>
					<td>6</td>
					<td>A</td>
				</tr>
				<tr class="odd gradeA">
					<td width="1%" class="f-s-600 text-inverse">5</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-5.jpg" class="img-rounded height-30" /></td>
					<td>Trident</td>
					<td>Internet Explorer 7</td>
					<td>Win XP SP2+</td>
					<td>7</td>
					<td>A</td>
				</tr>
				<tr class="even gradeA">
					<td width="1%" class="f-s-600 text-inverse">6</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-6.jpg" class="img-rounded height-30" /></td>
					<td>Trident</td>
					<td>AOL browser (AOL desktop)</td>
					<td>Win XP</td>
					<td>6</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">7</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-7.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Firefox 1.0</td>
					<td>Win 98+ / OSX.2+</td>
					<td>1.7</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">8</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-8.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Firefox 1.5</td>
					<td>Win 98+ / OSX.2+</td>
					<td>1.8</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">9</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-9.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Firefox 2.0</td>
					<td>Win 98+ / OSX.2+</td>
					<td>1.8</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">10</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-10.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Firefox 3.0</td>
					<td>Win 2k+ / OSX.3+</td>
					<td>1.9</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">11</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-11.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Camino 1.0</td>
					<td>OSX.2+</td>
					<td>1.8</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">12</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-12.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Camino 1.5</td>
					<td>OSX.3+</td>
					<td>1.8</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">13</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-13.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Netscape 7.2</td>
					<td>Win 95+ / Mac OS 8.6-9.2</td>
					<td>1.7</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">14</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-14.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Netscape Browser 8</td>
					<td>Win 98SE+</td>
					<td>1.7</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">15</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Netscape Navigator 9</td>
					<td>Win 98+ / OSX.2+</td>
					<td>1.8</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">16</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-2.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Mozilla 1.0</td>
					<td>Win 95+ / OSX.1+</td>
					<td>1</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">17</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-3.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Mozilla 1.1</td>
					<td>Win 95+ / OSX.1+</td>
					<td>1.1</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">18</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-4.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Mozilla 1.2</td>
					<td>Win 95+ / OSX.1+</td>
					<td>1.2</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">19</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-5.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Mozilla 1.3</td>
					<td>Win 95+ / OSX.1+</td>
					<td>1.3</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">20</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-6.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Mozilla 1.4</td>
					<td>Win 95+ / OSX.1+</td>
					<td>1.4</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">21</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-7.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Mozilla 1.5</td>
					<td>Win 95+ / OSX.1+</td>
					<td>1.5</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">22</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-8.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Mozilla 1.6</td>
					<td>Win 95+ / OSX.1+</td>
					<td>1.6</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">23</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-9.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Mozilla 1.7</td>
					<td>Win 98+ / OSX.1+</td>
					<td>1.7</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">24</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-10.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Mozilla 1.8</td>
					<td>Win 98+ / OSX.1+</td>
					<td>1.8</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">25</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-11.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Seamonkey 1.1</td>
					<td>Win 98+ / OSX.2+</td>
					<td>1.8</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">26</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-12.jpg" class="img-rounded height-30" /></td>
					<td>Gecko</td>
					<td>Epiphany 2.20</td>
					<td>Gnome</td>
					<td>1.8</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">27</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-13.jpg" class="img-rounded height-30" /></td>
					<td>Webkit</td>
					<td>Safari 1.2</td>
					<td>OSX.3</td>
					<td>125.5</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">28</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-14.jpg" class="img-rounded height-30" /></td>
					<td>Webkit</td>
					<td>Safari 1.3</td>
					<td>OSX.3</td>
					<td>312.8</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">29</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>
					<td>Webkit</td>
					<td>Safari 2.0</td>
					<td>OSX.4+</td>
					<td>419.3</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">30</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-2.jpg" class="img-rounded height-30" /></td>
					<td>Webkit</td>
					<td>Safari 3.0</td>
					<td>OSX.4+</td>
					<td>522.1</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">31</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-3.jpg" class="img-rounded height-30" /></td>
					<td>Webkit</td>
					<td>OmniWeb 5.5</td>
					<td>OSX.4+</td>
					<td>420</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">32</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-4.jpg" class="img-rounded height-30" /></td>
					<td>Webkit</td>
					<td>iPod Touch / iPhone</td>
					<td>iPod</td>
					<td>420.1</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">33</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-5.jpg" class="img-rounded height-30" /></td>
					<td>Webkit</td>
					<td>S60</td>
					<td>S60</td>
					<td>413</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">34</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-6.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Opera 7.0</td>
					<td>Win 95+ / OSX.1+</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">35</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-7.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Opera 7.5</td>
					<td>Win 95+ / OSX.2+</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">36</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-8.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Opera 8.0</td>
					<td>Win 95+ / OSX.2+</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">37</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-9.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Opera 8.5</td>
					<td>Win 95+ / OSX.2+</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">38</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-10.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Opera 9.0</td>
					<td>Win 95+ / OSX.3+</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">39</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-11.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Opera 9.2</td>
					<td>Win 88+ / OSX.3+</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">40</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-12.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Opera 9.5</td>
					<td>Win 88+ / OSX.3+</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">41</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-13.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Opera for Wii</td>
					<td>Wii</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">42</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-14.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Nokia N800</td>
					<td>N800</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">43</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>
					<td>Presto</td>
					<td>Nintendo DS browser</td>
					<td>Nintendo DS</td>
					<td>8.5</td>
					<td>C/A<sup>1</sup></td>
				</tr>
				<tr class="gradeC">
					<td width="1%" class="f-s-600 text-inverse">44</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-2.jpg" class="img-rounded height-30" /></td>
					<td>KHTML</td>
					<td>Konqureror 3.1</td>
					<td>KDE 3.1</td>
					<td>3.1</td>
					<td>C</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">45</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-3.jpg" class="img-rounded height-30" /></td>
					<td>KHTML</td>
					<td>Konqureror 3.3</td>
					<td>KDE 3.3</td>
					<td>3.3</td>
					<td>A</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">46</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-4.jpg" class="img-rounded height-30" /></td>
					<td>KHTML</td>
					<td>Konqureror 3.5</td>
					<td>KDE 3.5</td>
					<td>3.5</td>
					<td>A</td>
				</tr>
				<tr class="gradeX">
					<td width="1%" class="f-s-600 text-inverse">47</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-5.jpg" class="img-rounded height-30" /></td>
					<td>Tasman</td>
					<td>Internet Explorer 4.5</td>
					<td>Mac OS 8-9</td>
					<td>-</td>
					<td>X</td>
				</tr>
				<tr class="gradeC">
					<td width="1%" class="f-s-600 text-inverse">48</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-6.jpg" class="img-rounded height-30" /></td>
					<td>Tasman</td>
					<td>Internet Explorer 5.1</td>
					<td>Mac OS 7.6-9</td>
					<td>1</td>
					<td>C</td>
				</tr>
				<tr class="gradeC">
					<td width="1%" class="f-s-600 text-inverse">49</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-7.jpg" class="img-rounded height-30" /></td>
					<td>Tasman</td>
					<td>Internet Explorer 5.2</td>
					<td>Mac OS 8-X</td>
					<td>1</td>
					<td>C</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">50</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-8.jpg" class="img-rounded height-30" /></td>
					<td>Misc</td>
					<td>NetFront 3.1</td>
					<td>Embedded devices</td>
					<td>-</td>
					<td>C</td>
				</tr>
				<tr class="gradeA">
					<td width="1%" class="f-s-600 text-inverse">51</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-9.jpg" class="img-rounded height-30" /></td>
					<td>Misc</td>
					<td>NetFront 3.4</td>
					<td>Embedded devices</td>
					<td>-</td>
					<td>A</td>
				</tr>
				<tr class="gradeX">
					<td width="1%" class="f-s-600 text-inverse">52</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-10.jpg" class="img-rounded height-30" /></td>
					<td>Misc</td>
					<td>Dillo 0.8</td>
					<td>Embedded devices</td>
					<td>-</td>
					<td>X</td>
				</tr>
				<tr class="gradeX">
					<td width="1%" class="f-s-600 text-inverse">53</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-11.jpg" class="img-rounded height-30" /></td>
					<td>Misc</td>
					<td>Links</td>
					<td>Text only</td>
					<td>-</td>
					<td>X</td>
				</tr>
				<tr class="gradeX">
					<td width="1%" class="f-s-600 text-inverse">54</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-12.jpg" class="img-rounded height-30" /></td>
					<td>Misc</td>
					<td>Lynx</td>
					<td>Text only</td>
					<td>-</td>
					<td>X</td>
				</tr>
				<tr class="gradeC">
					<td width="1%" class="f-s-600 text-inverse">55</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-13.jpg" class="img-rounded height-30" /></td>
					<td>Misc</td>
					<td>IE Mobile</td>
					<td>Windows Mobile 6</td>
					<td>-</td>
					<td>C</td>
				</tr>
				<tr class="gradeC">
					<td width="1%" class="f-s-600 text-inverse">57</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-14.jpg" class="img-rounded height-30" /></td>
					<td>Misc</td>
					<td>PSP browser</td>
					<td>PSP</td>
					<td>-</td>
					<td>C</td>
				</tr>
				<tr class="gradeU">
					<td width="1%" class="f-s-600 text-inverse">58</td>
					<td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>
					<td>Other browsers</td>
					<td>All others</td>
					<td>-</td>
					<td>-</td>
					<td>U</td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- end panel-body -->
</div>
<!-- end row -->
<!-- begin row -->
{{-- <div class="row">
	<!-- begin col-4 -->
	<div class="col-xl-4 col-lg-6">
		<!-- begin card -->
		<div class="card border-0 bg-dark-darker text-white mb-3">
			<!-- begin card-body -->
			<div class="card-body"
				style="background: no-repeat bottom right; background-image: url(/assets/img/svg/img-4.svg); background-size: auto 60%;">
				<!-- begin title -->
				<div class="mb-3 text-grey">
					<b>SALES BY SOCIAL SOURCE</b>
					<span class="text-grey ml-2"><i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover"
							data-title="Sales by social source" data-placement="top"
							data-content="Total online store sales that came from a social referrer source."></i></span>
				</div>
				<!-- end title -->
				<!-- begin sales -->
				<h3 class="m-b-10">$<span data-animation="number" data-value="55547.89">0.00</span></h3>
				<!-- end sales -->
				<!-- begin percentage -->
				<div class="text-grey m-b-1"><i class="fa fa-caret-up"></i> <span data-animation="number"
						data-value="45.76">0.00</span>% increased</div>
				<!-- end percentage -->
			</div>
			<!-- end card-body -->
			<!-- begin widget-list -->
			<div class="widget-list widget-list-rounded inverse-mode">
				<!-- begin widget-list-item -->
				<a href="#" class="widget-list-item rounded-0 p-t-3">
					<div class="widget-list-media icon">
						<i class="fab fa-apple bg-indigo text-white"></i>
					</div>
					<div class="widget-list-content">
						<div class="widget-list-title">Apple Store</div>
					</div>
					<div class="widget-list-action text-nowrap text-grey">
						$<span data-animation="number" data-value="34840.17">0.00</span>
					</div>
				</a>
				<!-- end widget-list-item -->
				<!-- begin widget-list-item -->
				<a href="#" class="widget-list-item">
					<div class="widget-list-media icon">
						<i class="fab fa-facebook-f bg-blue text-white"></i>
					</div>
					<div class="widget-list-content">
						<div class="widget-list-title">Facebook</div>
					</div>
					<div class="widget-list-action text-nowrap text-grey">
						$<span data-animation="number" data-value="12502.67">0.00</span>
					</div>
				</a>
				<!-- end widget-list-item -->
				<!-- begin widget-list-item -->
				<a href="#" class="widget-list-item">
					<div class="widget-list-media icon">
						<i class="fab fa-twitter bg-aqua text-white"></i>
					</div>
					<div class="widget-list-content">
						<div class="widget-list-title">Twitter</div>
					</div>
					<div class="widget-list-action text-nowrap text-grey">
						$<span data-animation="number" data-value="4799.20">0.00</span>
					</div>
				</a>
				<!-- end widget-list-item -->
				<!-- begin widget-list-item -->
				<a href="#" class="widget-list-item">
					<div class="widget-list-media icon">
						<i class="fab fa-google bg-red text-white"></i>
					</div>
					<div class="widget-list-content">
						<div class="widget-list-title">Google Adwords</div>
					</div>
					<div class="widget-list-action text-nowrap text-grey">
						$<span data-animation="number" data-value="3405.85">0.00</span>
					</div>
				</a>
				<!-- end widget-list-item -->
				<!-- begin widget-list-item -->
				<a href="#" class="widget-list-item p-b-3">
					<div class="widget-list-media icon">
						<i class="fab fa-instagram bg-pink text-white"></i>
					</div>
					<div class="widget-list-content">
						<div class="widget-list-title">Instagram</div>
					</div>
					<div class="widget-list-action text-nowrap text-grey">
						$<span data-animation="number" data-value="0.00">0.00</span>
					</div>
				</a>
				<!-- end widget-list-item -->
			</div>
			<!-- end widget-list -->
		</div>
		<!-- end card -->
	</div>
	<!-- end col-4 -->
	<!-- end col-4 -->
	<!-- begin col-4 -->
	<div class="col-xl-4 col-lg-6">
		<!-- begin card -->
		<div class="card border-0 bg-dark text-white mb-3">
			<!-- begin card-body -->
			<div class="card-body">
				<!-- begin title -->
				<div class="mb-3 text-grey">
					<b>TOP PRODUCTS BY UNITS SOLD</b>
					<span class="ml-2"><i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover"
							data-title="Top products with units sold" data-placement="top"
							data-content="Products with the most individual units sold. Includes orders from all sales channels."></i></span>
				</div>
				<!-- end title -->
				<!-- begin product -->
				<div class="d-flex align-items-center m-b-15">
					<div class="widget-img rounded-lg width-30 m-r-10 bg-white p-3">
						<div class="h-100 w-100"
							style="background: url(/assets/img/product/product-8.jpg) center no-repeat; background-size: auto 100%;">
						</div>
					</div>
					<div class="text-truncate">
						<div>Apple iPhone XR (2019)</div>
						<div class="text-grey">$799.00</div>
					</div>
					<div class="ml-auto text-center">
						<div class="f-s-13"><span data-animation="number" data-value="195">0</span></div>
						<div class="text-grey f-s-10">sold</div>
					</div>
				</div>
				<!-- end product -->
				<!-- begin product -->
				<div class="d-flex align-items-center m-b-15">
					<div class="widget-img rounded-lg width-30 m-r-10 bg-white p-3">
						<div class="h-100 w-100"
							style="background: url(/assets/img/product/product-9.jpg) center no-repeat; background-size: auto 100%;">
						</div>
					</div>
					<div class="text-truncate">
						<div>Apple iPhone XS (2019)</div>
						<div class="text-grey">$1,199.00</div>
					</div>
					<div class="ml-auto text-center">
						<div class="f-s-13"><span data-animation="number" data-value="185">0</span></div>
						<div class="text-grey f-s-10">sold</div>
					</div>
				</div>
				<!-- end product -->
				<!-- begin product -->
				<div class="d-flex align-items-center m-b-15">
					<div class="widget-img rounded-lg width-30 m-r-10 bg-white p-3">
						<div class="h-100 w-100"
							style="background: url(/assets/img/product/product-10.jpg) center no-repeat; background-size: auto 100%;">
						</div>
					</div>
					<div class="text-truncate">
						<div>Apple iPhone XS Max (2019)</div>
						<div class="text-grey">$3,399</div>
					</div>
					<div class="ml-auto text-center">
						<div class="f-s-13"><span data-animation="number" data-value="129">0</span></div>
						<div class="text-grey f-s-10">sold</div>
					</div>
				</div>
				<!-- end product -->
				<!-- begin product -->
				<div class="d-flex align-items-center m-b-15">
					<div class="widget-img rounded-lg width-30 m-r-10 bg-white p-3">
						<div class="h-100 w-100"
							style="background: url(/assets/img/product/product-11.jpg) center no-repeat; background-size: auto 100%;">
						</div>
					</div>
					<div class="text-truncate">
						<div>Huawei Y5 (2019)</div>
						<div class="text-grey">$99.00</div>
					</div>
					<div class="ml-auto text-center">
						<div class="f-s-13"><span data-animation="number" data-value="96">0</span></div>
						<div class="text-grey f-s-10">sold</div>
					</div>
				</div>
				<!-- end product -->
				<!-- begin product -->
				<div class="d-flex align-items-center">
					<div class="widget-img rounded-lg width-30 m-r-10 bg-white p-3">
						<div class="h-100 w-100"
							style="background: url(/assets/img/product/product-12.jpg) center no-repeat; background-size: auto 100%;">
						</div>
					</div>
					<div class="text-truncate">
						<div>Huawei Nova 4 (2019)</div>
						<div class="text-grey">$499.00</div>
					</div>
					<div class="ml-auto text-center">
						<div class="f-s-13"><span data-animation="number" data-value="55">0</span></div>
						<div class="text-grey f-s-10">sold</div>
					</div>
				</div>
				<!-- end product -->
			</div>
			<!-- end card-body -->
		</div>
		<!-- end card -->
	</div>
	<!-- end col-4 -->
	<!-- begin col-4 -->
	<div class="col-xl-4 col-lg-6">
		<!-- begin card -->
		<div class="card border-0 bg-dark text-white mb-3">
			<!-- begin card-body -->
			<div class="card-body">
				<!-- begin title -->
				<div class="mb-3 text-grey">
					<b>MARKETING CAMPAIGN</b>
					<span class="ml-2"><i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover"
							data-title="Marketing Campaign" data-placement="top"
							data-content="Campaign that run for getting more returning customers."></i></span>
				</div>
				<!-- end title -->
				<!-- begin row -->
				<div class="row align-items-center p-b-1">
					<!-- begin col-4 -->
					<div class="col-4">
						<div class="height-100 d-flex align-items-center justify-content-center">
							<img src="/assets/img/svg/img-2.svg" class="mw-100 mh-100" />
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-8 -->
					<div class="col-8">
						<div class="m-b-2 text-truncate">Email Marketing Campaign</div>
						<div class="text-grey m-b-2 f-s-11">Mon 12/6 - Sun 18/6</div>
						<div class="d-flex align-items-center m-b-2">
							<div class="flex-grow-1">
								<div class="progress progress-xs rounded-corner bg-white-transparent-1">
									<div class="progress-bar progress-bar-striped bg-indigo" data-animation="width" data-value="80%"
										style="width: 0%"></div>
								</div>
							</div>
							<div class="ml-2 f-s-11 width-30 text-center"><span data-animation="number" data-value="80">0</span>%
							</div>
						</div>
						<div class="text-grey f-s-11 m-b-15 text-truncate">
							57.5% people click the email
						</div>
						<a href="#" class="btn btn-xs btn-indigo f-s-10 pl-2 pr-2">View campaign</a>
					</div>
					<!-- end col-8 -->
				</div>
				<!-- end row -->
				<hr class="bg-white-transparent-2 m-t-20 m-b-20" />
				<!-- begin row -->
				<div class="row align-items-center">
					<!-- begin col-4 -->
					<div class="col-4">
						<div class="height-100 d-flex align-items-center justify-content-center">
							<img src="/assets/img/svg/img-3.svg" class="mw-100 mh-100" />
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-8 -->
					<div class="col-8">
						<div class="m-b-2 text-truncate">Facebook Marketing Campaign</div>
						<div class="text-grey m-b-2 f-s-11">Sat 10/6 - Sun 18/6</div>
						<div class="d-flex align-items-center m-b-2">
							<div class="flex-grow-1">
								<div class="progress progress-xs rounded-corner bg-white-transparent-1">
									<div class="progress-bar progress-bar-striped bg-warning" data-animation="width" data-value="60%"
										style="width: 0%"></div>
								</div>
							</div>
							<div class="ml-2 f-s-11 width-30 text-center"><span data-animation="number" data-value="60">0</span>%
							</div>
						</div>
						<div class="text-grey f-s-11 m-b-15 text-truncate">
							+124k visitors from facebook
						</div>
						<a href="#" class="btn btn-xs btn-warning f-s-10 pl-2 pr-2">View campaign</a>
					</div>
					<!-- end col-8 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end card-body -->
		</div>
		<!-- end card -->
	</div>
	<!-- end col-4 -->
</div> --}}
<!-- end row -->
@endsection


@push('scripts')
<script src="/assets/plugins/d3/d3.min.js"></script>
<script src="/assets/plugins/nvd3/build/nv.d3.js"></script>
<script src="/assets/plugins/jvectormap-next/jquery-jvectormap.min.js"></script>
<script src="/assets/plugins/jvectormap-next/jquery-jvectormap-world-mill.js"></script>
<script src="/assets/plugins/apexcharts/dist/apexcharts.min.js"></script>
<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/assets/js/demo/dashboard-v3.js"></script>
@endpush

@push('scripts')
<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables.net-fixedcolumns/js/dataTables.fixedcolumns.min.js"></script>
<script src="/assets/plugins/datatables.net-fixedcolumns-bs4/js/fixedcolumns.bootstrap4.min.js"></script>
<script src="/assets/js/demo/table-manage-fixed-columns.demo.js"></script>
@endpush