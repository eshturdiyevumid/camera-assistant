<?php

namespace App\Traits;

use App\Models\Employee\Employee;

trait HasEmployee
{
  public function employee()
  {
    return $this->belongsTo(Employee::class);
  }
}
