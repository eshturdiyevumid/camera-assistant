<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreHistoryRequest;
use App\Models\Employee\Employee;
use App\Models\Employee\EmployeeHistory;
use App\Models\Organization\Organization;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function getEmployees($organization_key)
    {
        $organization = Organization::where('device_key', $organization_key)->first();
        if (is_null($organization)) {
            return response()->json([]);
        }
        return response()->json(Employee::getEmployees($organization->id));
    }

    public function storeHistory(StoreHistoryRequest $request)
    {
        return EmployeeHistory::store($request->validated());
    }
}
