<?php

namespace App\Http\Controllers;

use App\Models\Employee\Employee;
use App\Models\Employee\EmployeeHistory;
use App\Models\Employee\EmployeeReport;
use App\Models\Employee\Wanted;
use App\Models\Employee\WantedHistory;
use App\Models\Organization\Territory;
use Illuminate\Http\Request;

class CommonController extends Controller
{

    public function index()
    {
        $list = EmployeeHistory::whereHas('employee', function ($query) {
            $query->where('organization_id', auth()->user()->organization_id);
        })->get();
        return view('project/dashboard', compact('list'));
    }

    public function employees(Request $request)
    {
        $list = Employee::where('organization_id', auth()->user()->organization_id)->get();
        return view('project/employees', compact('list'));
    }

    public function territories(Request $request)
    {
        $list = Territory::where('organization_id', auth()->user()->organization_id)->get();
        return view('project/territories', compact('list'));
    }

    public function wanteds(Request $request)
    {
        $list = Wanted::whereHas('employee', function ($query) {
            $query->where('organization_id', auth()->user()->organization_id);
        })->latest('id')->get();
        $employees = Employee::where('organization_id', auth()->user()->organization_id)->get();

        return view('project/wanteds', compact('list', 'employees'));
    }
    public function report(Request $request)
    {
        $list = EmployeeReport::whereHas('employee', function ($query) {
            $query->where('organization_id', auth()->user()->organization_id);
        })->get();
        return view('project/report', compact('list'));
    }

    public function history($id)
    {
        $wanted = Wanted::find($id);
//        $employee = Employee::find($id);
        return view('project/employee-history', compact('wanted'));
    }

    public function stopSearching($id)
    {
        $model = Wanted::find($id);
        $model->status = false;
        $model->save();
        return back()->with('warning', 'Stopped Searching for ' . $model->employee->name);
    }

    public function search(Request $request)
    {
        $model = Wanted::create([
            'employee_id' => $request->id,
            'organization_id' => auth()->user()->organization_id,
            'status' => true,
            'date' => date('Y-m-d')
        ]);
        return back()->with('success', 'Started Searching for ' . $model->employee->name);
    }

    public function employeeAdd(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required',
        ]);
        $image = $this->uploadImage($request->file('image'), 'user');
        $model = Employee::create([
            'name' => $request->name,
            'image' => $image,
            'organization_id' => auth()->user()->organization_id
        ]);

        return back()->with('success', 'New Employee is added: ' . $model->name);
    }

    function uploadImage($file, $path, $old = null)
    {
        if ($old != null && file_exists(public_path() . $old)) {
            unlink(public_path() . $old);
        }
        $names = explode(".", $file->getClientOriginalName());
        $image = time() . '.' .  $names[count($names) - 1];
        $file->storeAs("public/$path", $image);
        return "/storage/$path/" . $image;
    }
}
