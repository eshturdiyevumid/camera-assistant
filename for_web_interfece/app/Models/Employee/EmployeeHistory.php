<?php

namespace App\Models\Employee;

use App\Models\Organization\Device;
use App\Traits\HasEmployee;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeHistory extends Model
{
    use HasFactory, HasEmployee;
    protected $guarded = [];

    public static function store($data)
    {
        $organization_key = $data['organization_key'];
        unset($data['organization_key']);
        $device = Device::find($data['device_id']);
        $day = date('Y-m-d');
        if ($device->action_type != 'free') {
            $history = self::where([
                'employee_id' => $data['employee_id'],
            ])->whereDate('action_date', $day)->orderBy('created_at', 'desc')->first();
            if ($history) {
                if ($history->action_type != $device->action_type) {
                    $data['action_date'] = date('Y-m-d H:i:s');
                    $data['action_type'] = $device->action_type;
                    self::create($data);
                    if ($device->action_type == 'output') {
                        $hour = date_diff(date_create($data['action_date']), date_create($history->action_date));
                        $work = round(($hour->d)*24 + $hour->h + ($hour->i)/60 + ($hour->s)/3600, 2);
                        $employee_report = EmployeeReport::where([
                            'employee_id' => $data['employee_id'],
                            'date' => $day
                        ])->first();
                        if ($employee_report) {
                            $employee_report->update(['work' => ($employee_report->work + $work)]);
                        } else {
                            EmployeeReport::create([
                                'employee_id' => $data['employee_id'],
                                'date' => $day,
                                'work' => $work
                            ]);
                        }
                    }
                }
            } else {
                $data['action_date'] = date('Y-m-d H:i:s');
                $data['action_type'] = $device->action_type;
                self::create($data);
//                if ($device->action_type == 'output') {
//                    $work = $data['action_date'] - $history->action_date;
//                    $employee_report = EmployeeReport::where([
//                        'employee_id' => $data['employee_id'],
//                        'date' => $day
//                    ])->first();
//                    if ($employee_report) {
//                        $employee_report->update(['work' => ($employee_report->work + $work)]);
//                    } else {
//                        EmployeeReport::create([
//                            'employee_id' => $data['employee_id'],
//                            'date' => $day,
//                            'work' => $work
//                        ]);
//                    }
//                }
            }

        }

        $wanted = Wanted::where([
            'employee_id' => $data['employee_id'],
            'date' => $day,
            'status' => true,
        ])->first();

        if ($wanted) {
            $wanted_history = WantedHistory::where([
                'wanted_id' => $wanted->id,
                'device_id' => $data['device_id']
                ])->orderBy('created_at', 'desc')->first();
            if (date_diff(date_create(date('Y-m-d H:i:s')), date_create($wanted_history->action_date))->i > 2) {
                WantedHistory::create([
                    'wanted_id' => $wanted->id,
                    'device_id' => $data['device_id'],
                    'action_date' => date('Y-m-d H:i:s')
                ]);
            }
        }
    }

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
}
