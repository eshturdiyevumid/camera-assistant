<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function getImageAttribute($value)
    {
        if ($value == null) {
            return null;
        }
        return asset($value);
    }

    public static function getEmployees($organization_id)
    {
        return self::where('organization_id', $organization_id)->get();
    }

    public function history()
    {
        return $this->hasMany(EmployeeHistory::class);
    }

    public function wanted(){
        return $this->belongsTo(Wanted::class);
    }
}
