<?php

namespace App\Models\Employee;

use App\Models\Organization\Device;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WantedHistory extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function device(){
        return $this->belongsTo(Device::class);
    }

}
