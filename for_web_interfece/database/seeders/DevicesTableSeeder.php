<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('devices')->delete();

        \DB::table('devices')->insert(array (
            0 =>
                array (
                    'organization_id' => 1,
                    'territory_id' => 1,
                    'name' => 'Камера 001',
                    'action_type' => 'input',
                ),
        ));
    }
}
