<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TerritoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('territories')->delete();

        \DB::table('territories')->insert(array (
            0 =>
                array (
                    'name' => 'Главный вход',
                    'organization_id' => 1,
                    'point_x' => '41.340908',
                    'point_y' => '69.286539',
                ),
        ));
    }
}
