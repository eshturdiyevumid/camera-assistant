<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OrganizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('organizations')->delete();

        \DB::table('organizations')->insert(array (
            0 =>
                array (
                    'name' => 'Kibera',
                    'device_key' => 'kibera',
                ),
        ));
    }
}
