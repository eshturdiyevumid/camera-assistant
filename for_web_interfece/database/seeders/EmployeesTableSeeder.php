<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('employees')->delete();

        \DB::table('employees')->insert(array (
            0 =>
                array (
                    'organization_id' => 1,
                    'name' => 'Eshturdiev Umidjon',
                ),
        ));
    }
}
